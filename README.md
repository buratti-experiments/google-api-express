# How to clone de project and start the container docker

## Clone the project

```console
git clone git@gitlab.com:buratti-experiments/google-api-express.git
```

## Install the packages of dependencies of the project

```console
npm i
```

## Create image docker locale

```console
docker build -t registry.gitlab.com/buratti-experiments/google-api-express .
```

## Run container docker

```console
docker run -it -p 900:3000 -v $(pwd):/app registry.gitlab.com/buratti-experiments/google-api-express
```

---

# Create project with structure node

```console
npm init -y
```

# Install the packages of dependencies of the your project

```console
npm i
```

# Create the file `Dockerfile` with the configurations


```yaml
FROM node:9-slim
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD [ "npm", "start" ]
```

# Create de image docker

```console
docker build -t <project-name-here> .
```

# Run your container docker

```console
docker run -it -p 900:3000 -v $(pwd):/app <project-name-here>
```
